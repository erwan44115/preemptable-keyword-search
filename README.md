# Preemptable keyword search for SaGe

This is a solution to have a fast and constant keyword search for SaGe. SaGe is a SPARQL query engine for public Linked Data providers that implements Web preemption, see : https://github.com/sage-org/sage-engine for more information.

## Implement the keyword search for a dataset 

To implement our solution, we must install sage-engine and write an adapted configuration file like in the [sage-engine documentation](https://sage-org.github.io/sage-engine/) in order  to have a PostgreSQL database created by SaGe engine like describe in the documentation.
We have then all the elements to open the server and execute the Python script which implements our solution.

    python keywordSearch.py [NAME OF THE POSTGRE BASE] [NAME OF THE NEW BASE]
    
## How it works

To explain how our Python script operate, first, it takes each Subject, Object of the swdf triples and applied the ts_vector function for the objects.
The ts_vector function is used for full text search in PostgreSQL and acts like a parser. Therefore, for a giving text and a language it will return an ordered list of the words in the text if they correspond to the giving language. So, if no words correspond to the English language in our example, all are ignored. Moreover, the function filters stop words which are very common words like a,the or it for an English text. Finally, it reduces also each word to their lexeme. In clear, ts_vector will truncate plurals and suffixes for each word and this word base is called a lexeme.
So, for one triple we insert each lexeme selected by the ts_vector into the Subject column of the swdfappearin table, with a new predicate named appearIn and the Object is none other than the Subject of the swdf triple. Indeed, this is the principle of an inverted index.
The Python script also checks for no duplicate triple if more than one lexeme is presented for the same subject.
It’s important to notice that we do not need to create a B-tree index because SaGe creates them automatically applied to Subject, Predicate and Object.
If our solution works, we know that we can use a keyset pagination to paginate because we use a B-tree index and we can have a following result in constant time. But to be sure that the access of a following result using a GIN index to paginate is not better, we will make an experiment testing if execution times slow down according to the number of results for another dataset that contains much more triples to have interesting results.
