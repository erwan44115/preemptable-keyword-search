import psycopg2
import re

conn = psycopg2.connect(database="postgres",
                        user="postgres",
                        host="localhost",
                        password="postgres",
                        port="5432")

cur = conn.cursor()

cur.execute("SELECT subject,to_tsvector('english',object) from swdf")

row = cur.fetchone()

while row is not None:
    words = re.findall("\w+", row[1]) # pour prendre chaque mot du résultat du ts_vector
    for word in words:
        if word[0:4]!='http' and word[0]!='_':
            if not word.isdigit(): #enlever les numéros du ts_vector
                with conn:
                    with conn.cursor() as cursorexist: # When a connection exits the with block, if no exception has been raised by the block, the transaction is committed.
                        cursorexist.execute("SELECT subject,object FROM swdfAppearIn WHERE subject=%s and object=%s",(word,row[0]))
                        exist = cursorexist.fetchone()
                if not exist:
                    with conn:
                        with conn.cursor() as cursorinsert:
                            cursorinsert.execute("INSERT INTO swdfAppearIn (subject,predicate,object) VALUES (%s,%s,%s)",(word,'appearIn',row[0]))
                            #print("INSERT INTO swdfAppearIn (subject,predicate,object) VALUES ('%s','%s','%s')" % (word,'appearIn',row[0]))

    row = cur.fetchone()

cur.close()
conn.close()
