import requests

# méthode avec CURL, marche mais peut pas mettre de limit car le limit dans sage est mis dans la commande shell

#     'Accept': 'application/json',
# }
#
# paramsReqComplete = (
#     ('query', 'SELECT ?po WHERE{ <ns> <appearIn> ?o}'),
#     ('default-graph-uri', 'bsbm1k'),
# )
#
# paramsReqFirstResult = (
#     ('query', 'SELECT ?po WHERE{ <ns> <appearIn> ?o}'),
#     ('default-graph-uri', 'bsbm1k'),
# )
#
# response = requests.get('http://0.0.0.0:8000/sparql', headers=headers, params=params)
#
# print(response.text)
# print(response.elapsed.total_seconds())


# méthode avec OS, marche --> on peut calculer le temps que ça met
def queryRun(limit,haveLimit=True):
    import os
    if haveLimit :
        stream = os.popen('poetry run sage-query ../configSage/configAppearIn.yaml swdfappearin -f ../TER/test_perf/test_on_SPO/query/query.sparql -l '+str(limit))
    else :
        stream = os.popen('poetry run sage-query ../configSage/configAppearIn.yaml swdfappearin -f ../TER/test_perf/test_on_SPO/query/query.sparql')
    output = stream.read()
    if haveLimit is not True :
        filename = "../TER/test_perf/test_on_SPO/result/resultAppearIn.txt"
        fichier = open(filename, "w")
        fichier.write(output)
        fichier.close()
        noDoublon = []

        with open(filename, 'r') as file:
            # lire le fichier ligne par ligne
            for line in file:
                # copier la ligne dans la liste si elle n'y est pas déjà
                if line not in noDoublon:
                    noDoublon.append(line)

        # réouvrir le fichier mais en mode écriture (ce qui effacera le contenu existant) et écrire les lignes de la liste
        with open(filename, 'w') as file:
            for line in noDoublon:
                file.write(line)
#    print(haveLimit)

def queryRunRegex(limit,haveLimit=True):
    import os
    if haveLimit :
        stream = os.popen('poetry run sage-query ../configSage/config.yaml swdf -f ../TER/test_perf/test_on_SPO/query/regex.sparql -l '+str(limit))
    else :
        stream = os.popen('poetry run sage-query ../configSage/config.yaml swdf -f ../TER/test_perf/test_on_SPO/query/regex.sparql ')
    output = stream.read()
    if haveLimit is not True:
        filename = "../TER/test_perf/test_on_SPO/result/resultRegex.txt"
        fichier = open(filename, "w")
        fichier.write(output)
        fichier.close()
        noDoublon = []

        with open(filename, 'r') as file:
            # lire le fichier ligne par ligne
            for line in file:
                # copier la ligne dans la liste si elle n'y est pas déjà
                if line not in noDoublon:
                    noDoublon.append(line)

        # réouvrir le fichier mais en mode écriture (ce qui effacera le contenu existant) et écrire les lignes de la liste
        with open(filename, 'w') as file:
            for line in noDoublon:
                file.write(line)
    #print(haveLimit)


# méthode avec subprocess --> pareil que OS plus ou moins

# import subprocess
# process = subprocess.Popen(['poetry', 'run','sage-query','http://localhost:8000/sparql','bsbm1k','-f','../configSage/query/regex.sparql','-l','10'],
#                            stdout=subprocess.PIPE,
#                            universal_newlines=True)
# stdout = process.communicate()
# print(stdout)
# tableau des taills
# base unqieuement avec label
# faire le regex sur une autre base
# filter contains en plus

# en plus : gin index -> regex requete --> pagination limit offset groupby ou order by

if __name__ == '__main__':
    import timeit
    import matplotlib.pyplot as plt

    timeQuery = []
    timeQueryRegex = []
    limit = [1,5,10,20,30,40,50,60,70,80,90,100,110,120]
    for i in limit:
        timeQuery.append(timeit.timeit("queryRun("+str(i)+")", setup="from __main__ import queryRun",number=1))
        timeQueryRegex.append(timeit.timeit("queryRunRegex("+str(i)+")", setup="from __main__ import queryRunRegex",number=1))

    print("Query time with appearIn")
    print(timeQuery)
    print("Query time with regex")
    print(timeQueryRegex)
    plt.plot(limit, timeQuery,label='temps de appearIn')
    plt.plot(limit,timeQueryRegex,label="temps avec REGEX")
    plt.xlabel("nombre de résultats demandés")
    plt.ylabel("temps en secondes")
    plt.title("Temps d'éxecution d'une requête sparql avec appearIn et regex")
    plt.savefig('../TER/test_perf/test_on_SPO/result/perf.png')

    # calcul total sans limit
    totalQueryTime = timeit.timeit("queryRun(0,False)", setup="from __main__ import queryRun",number=1)
    totalQueryTimeRegex = timeit.timeit("queryRunRegex(0,False)", setup="from __main__ import queryRunRegex",number=1)
    print("total query time : "+str(totalQueryTime))
    print("total query time with REGEX : "+str(totalQueryTimeRegex))
